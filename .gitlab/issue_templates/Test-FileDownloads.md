**Matrix Name:** [matrix_name]

**Matrix Version:** [matrix_version]

**Matrix Release:** [3.6.x.x]

**App Name:** [app_name]

**Error Response:** [error_response]

**Error Output:** [error_output]

**PowerShell Version:** [`($PSVersionTable).PSVersion.ToString()`]

**Possible Fixes:** [possible_fixes]

/label ~bug ~matrix-dload-error ~needs-investigation