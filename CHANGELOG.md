# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.2.5.7] - 2020-07-03
### Added
- `Arch` to manifest.
- Retroactively added `Arch` to all existing manifests.

## [4.2.4.6] - 2020-07-03
### Added
- Additional language support for installers per manifest.
- Option to expand into NuSpec where Release was previously.
- SHA512 checksum to installers.

### Changed
- Manifest version from 4.2.x.x to 4.2.4.6 formally.

### Removed
- Legacy manifests.
- Release of Manifest.

