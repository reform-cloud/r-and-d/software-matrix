# Software Matrix
                    
[![pipeline status](https://gitlab.com/reform-cloud/r-and-d/software-matrix/badges/master/pipeline.svg)](https://gitlab.com/reform-cloud/r-and-d/software-matrix/commits/master)
![GitHub top language](https://img.shields.io/github/languages/top/repasscloud/software-library?logo=powershell)
![Repository size](https://img.shields.io/github/repo-size/repasscloud/software-library.svg)
![patch/20](https://github.com/repasscloud/software-library/workflows/CI/badge.svg?branch=patch%2F20)
[![GitHub release (latest by date)](https://img.shields.io/github/v/release/repasscloud/software-library)](https://github.com/repasscloud/software-library/releases/latest)
![GitHub commits since latest release (by date)](https://img.shields.io/github/commits-since/repasscloud/software-library/latest)
![License](https://img.shields.io/github/license/repasscloud/software-library.svg)


```json
{
    "Category": "",
    "Manifest": "4.2.4.6",
    "Nuspec": false,
    "Copyright": "",
    "Id": {
      "Version": "",
      "Name": "",
      "Publisher": "",
      "AppCopyright": "",
      "License": "",
      "LicenseURL": "",
      "Tags": [],
      "Description": "",
      "Homepage": "",
      "Arch": [],
      "Languages": [],
      "Depends": [],
      "Installers": {
        "x64": {
          "en-US": {
            "Url": "",
            "FollowUrl": "",
            "Sha256": "",
            "Sha512": "",
            "InstallerType": "",
            "InstallSwitches": "",
            "UninstallString": "",
            "UpdateURI": "",
            "UpdateRegex": ""
          }
        },
        "x86": {
          "en-US": {
            "Url": "",
            "FollowUrl": "",
            "Sha256": "",
            "Sha512": "",
            "InstallerType": "",
            "InstallSwitches": "",
            "UninstallString": "",
            "UpdateURI": "",
            "UpdateRegex": ""
          }
        }
      }
    }
  }
      
```
